#include <stdio.h>
#define MAX 100

struct details{
    char name[MAX];
    char subject[MAX];
    int marks;
};

int main()
{
    int n=0;
    printf("Enter the number of students - ");
    scanf("%d", &n);

    struct details student[MAX];

    for(int i=0; i<n; i++)
    {
        printf("\n");
        printf("Enter the name of the Student - ");
        scanf("%s", &student[i].name);

        printf("Enter subject name - ");
        scanf("%s", &student[i].subject);

        printf("Enter the marks - ");
        scanf("%d", &student[i].marks);
    }

    printf("\n");

    for(int i=0; i<n; i++)
    {
        printf("%s's marks : \n\t%s - %d\n\n", student[i].name, student[i].subject, student[i].marks);
    }

    return 0;
}
